#include <iostream>
#include <string>

int main(void)
{
    auto mystring{"This is a string"};
    auto myint{99};

    std::cout << mystring << std::endl;
    std::cout << myint << std::endl;
    
    return 0;
}
