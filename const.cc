#include <iostream>
#include <string>
using namespace std;

char *crashC()
{
    return (char*)"Some text";
}
string crash()
{
    return "Some text";
}
const char *fixC()
{
    return (char*)"Some text";
}
void cMethod1(int &Parameter1) 
{ 
    Parameter1++; 
}
void cMethod2(int *Parameter1) 
{ 
    (*Parameter1)++; 
}
void cppMethod( int const &Parameter1 )
{
    std::cout << Parameter1 << ": cannot modify inside function" << std::endl;
    // This is illegal becauase const protects it
    //Parameter1++;
}
class Class1
{ 
public:
    Class1(int temp = 1): MemberVariable1(temp) {}
    void Method1();
private:
    int MemberVariable1;
};
void Class1::Method1()
{ 
    std::cout << MemberVariable1 << ": before" << std::endl;
    MemberVariable1=MemberVariable1+1;
    std::cout << MemberVariable1 << ": after" << std::endl;
}
class Class2
{ 
public:
    Class2(int temp = 1): MemberVariable1(temp) {}
    void Method1() const;
private:
    int MemberVariable1;
};
void Class2::Method1() const
{ 
    std::cout << "void Method1() const;" << std::endl;
    std::cout << "void Class2::Method1() const" << std::endl;
    std::cout << MemberVariable1 << ": cannot change the variable now is protected with const" << std::endl;
    // This is now illegal cannot change this guy
    //MemberVariable1=MemberVariable1+1;
}
int main(void)
{
    /*
     * Basically ‘const’ applies to whatever is on its immediate left 
     * (other than if there is nothing there in which case it applies to whatever is its immediate right).
     */

    /*
     * The simplest use is to declare a named constant, const int. 
     * This was available in the ancestor of C++, C.
     */
    const int oldCConst = 96;
    std::cout << "# The simplest use is to declare a named constant, const int" << std::endl;
    std::cout << "-> const int oldCConst" << std::endl;
    std::cout << oldCConst << std::endl;
    std::cout << std::endl;

    /*
     *varPtr2ConstInt is a variable pointer to a constant integer and
     */
    std::cout << "# varPtr2ConstInt is a variable pointer to a constant integer" << std::endl;
    std::cout << "-> const int *varPtr2ConstInt" << std::endl;
    const int *varPtr2ConstInt;
    varPtr2ConstInt = &oldCConst;
    std::cout << *varPtr2ConstInt << std::endl;
    std::cout << "I'm going to change what varPtr2ConstInt points to, another const int " << std::endl;
    const int oldCConst2 = 10;
    varPtr2ConstInt = &oldCConst2;
    std::cout << *varPtr2ConstInt << std::endl;
    std::cout << std::endl;

    /*
     *contPtr2VarInt is constant pointer to a variable integer
     */
    std::cout << "# contPtr2VarInt is constant pointer to a variable integer" << std::endl;
    std::cout << "-> int *const contPtr2VarInt" << std::endl;
    int varInt = 52;
    int *const contPtr2VarInt = &varInt;
    std::cout << *contPtr2VarInt << std::endl;
    std::cout << "Now I'm going to change the value of variable integer" << std::endl;
    varInt = 2;
    std::cout << *contPtr2VarInt << std::endl;
    std::cout << std::endl;

    /*
     *constPtr2ConstInt is constant pointer to a constant integer.
     */
    std::cout << "# constPtr2ConstInt is constant pointer to a constant integer" << std::endl;
    std::cout << "-> int const *const constPtr2ConstInt" << std::endl;
    int const *const constPtr2ConstInt = &oldCConst;
    std::cout << *constPtr2ConstInt << std::endl;
    std::cout << std::endl;
    
    std::cout << "# Const functions" << std::endl;
    std::cout << "This is useful for returning constant strings and arrays from functions which the program could otherwise try to alter and crash" << std::endl;
    std::cout << "This is fine with string cpp syntax" << std::endl;
    char temp = crash()[1]='a';
    std::cout <<  temp << std::endl;

    std::cout << "But breaks in C syntax (need to uncomment)" << std::endl;
    //temp = crashC()[1]='a';
    //std::cout <<  temp << std::endl;

    std::cout << "Const used in C syntax to break at compile time with messages (need to uncomment)" << std::endl;
    std::cout << "const char *fixC() vs char *crashC()" << std::endl;
    //temp = fixC()[1]='a';
    //std::cout <<  temp << std::endl;
    std::cout << std::endl;


    std::cout << "# Const in function parameter" << std::endl;
    std::cout << "variable to be passed without copying without risk of it being altered." << std::endl;
    int integer = 0;
    std::cout << integer << ": Initial value" << std::endl;
    cMethod1(integer);
    std::cout << integer << ": cMethod1(integer); --> void cMethod1(int &Parameter1) " << std::endl;
    cMethod2(&integer);
    std::cout << integer << ": cMethod2(&integer); --> void cMethod2(int *Parameter1)" << std::endl;
    cppMethod(integer);
    std::cout << integer << ": cppMethod(integer); --> void cppMethod( int const &Parameter1 )" << std::endl;
    std::cout << std::endl;

    std::cout << "# Const in class methods" << std::endl;
    Class1 javier;
    javier.Method1();
    Class2 fraga;
    fraga.Method1();

    return 0;
}
