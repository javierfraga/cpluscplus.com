#include <iostream>
using namespace std;

namespace first
{
    int x = 5;
    int y = 10;
}

namespace second
{
    double x = 3.1416;
    double y = 2.7183;
}

int main () {

    {
        using namespace first;
        std::cout << x << std::endl;
        std::cout << y << std::endl;
    }
    std::cout << std::endl;
    {
        using namespace second;
        std::cout << x << std::endl;
        std::cout << y << std::endl;
    }
    std::cout << std::endl;

    // after using in this scope nullfies in sub-scopes made
    // like the ones above
    using first::x;
    using second::y;
    cout << x << '\n';
    cout << y << '\n';
    std::cout << std::endl;
    cout << first::y << '\n';
    cout << second::x << '\n';
    std::cout << std::endl;


    return 0;
}
