#include <iostream>

template <class T>
T sum( T a , T b )
{
    T result;
    result = a + b;
    return result;
}

template <class T , class U>
bool are_equal( T a , U b )
{
    return ( a == b );
}

int main(void)
{
    int i = 5 , j = 6 , k;
    double f = 2.0 , g = 0.5 , h;

    //k = sum<int>( i , j );
    //h = sum<double>( f , g );

    //It is possible to instead simply write:
    k = sum( i , j );
    h = sum( f , g );
    //If sum is called with arguments of different types, the compiler may not be able to deduce the type of T automatically.
    
    std::cout << k << std::endl;
    std::cout << h << std::endl;

    if ( are_equal<int,double>( 10 , 10.0 ) ) {
    //if ( are_equal( 10 , 10.0 ) ) {
        std::cout << "x and y are equal" << std::endl;
    } else {
        std::cout << "x and y are not equal" << std::endl;
    }
    
    return 0;
}
