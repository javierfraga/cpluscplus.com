#include <iostream>
#include <string>
using namespace std;

int main(void)
{
    string javier = "this forms a" "string" " of characters";
    string fraga = "this string \
                    continues on another line";
    std::cout << javier << std::endl;
    std::cout << fraga << std::endl;

    std::cout << R"(better way to use special characters in strings)" << std::endl;
    std::cout << R"(forces all characters in string, like these "" '' \n, to be constant)" << std::endl;
    auto santos(R"(string with \backslash)");
    std::cout << santos << std::endl;

    return 0;
}
